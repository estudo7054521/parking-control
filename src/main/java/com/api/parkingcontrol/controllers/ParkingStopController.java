package com.api.parkingcontrol.controllers;

import com.api.parkingcontrol.services.ParkingSpotService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/parking-spot")
public class ParkingStopController {

    //Ponto de injeção da service via contrutor
    final ParkingSpotService parkingSpotService;

    public ParkingStopController(ParkingSpotService parkingSpotService) {
        this.parkingSpotService = parkingSpotService;
    }



}
